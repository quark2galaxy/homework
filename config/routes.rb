Rails.application.routes.draw do
  #get 'index/top'
  #get 'bbs/top'
  root 'index#top'
  get '/bbs', to:'bbs#top', as:'bbs'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/user_new', to:'bbs#user_new', as:'user_new'
  post '/user_create', to:'bbs#user_create', as:'user_create'
  get '/user_login', to:'bbs#user_login', as: 'user_login'
  post '/user_confirm', to:'bbs#user_confirm', as:'user_confirm'
  get '/user_top', to:'bbs#user_top', as: 'user_top'
  get '/user_logout', to:'bbs#user_logout', as: 'user_logout'
  post '/post_new', to:'bbs#post_new', as:'post_new'
  get '/reply', to: 'bbs#reply', as:'reply'
  post '/comment', to:'bbs#comment', as:'comment'
end
