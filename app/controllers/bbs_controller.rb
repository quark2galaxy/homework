require 'date'
class BbsController < ApplicationController
  def top
    @users = User.all
  end
  def user_new
    @user = User.new
  end
  def user_create
    #time = Time.now
    @user = User.new(name: params[:name], mail: params[:mail], password: params[:pwd])
    if @user.save
      redirect_to(user_login_path)
    else
      render 'new' and return
    end
  end
  def user_login
    @post = Post.new
  end
  def user_confirm
     @user = User.find_by(mail: params[:mail], password: params[:pwd])
    if @user
      #flash[:notice] = "ログインしました"
      session[:user_id] = @user.id
      redirect_to(user_top_path)
      #puts 'ヒットしました！'
      
    else
      #puts 'いませんでした'
      render("user_login")
    end
  end
  def user_top
   @user = User.find_by_id(session[:user_id])
   #@posts = Post.all
   #@posts = Post.joins(:user)
   #@users = User.joins(:posts)
   #@users = User.joins(:posts).select('users.name as name, posts.title as title, posts.body as body')
   @posts = Post.joins(:user).select('posts.id as post_id, users.name as name, posts.title as title, posts.body as body, posts.created_at as created_at')
  end
  def user_logout
    session[:user_id] = nil
    redirect_to(user_login_path)
  end
  
  def post_new
     @post = Post.new(user_id: session[:user_id], title: params[:title], body: params[:body])
    if @post.save
      redirect_to(user_top_path)
    else
      render 'post_new' and return
    end
  end
  def reply
    @post_id = params[:post_id]
    @posts = Post.joins(:user).select('posts.id as post_id, users.name as name, posts.title as title, posts.body as body').where('posts.id = ?', @post_id)
    @comment = Reply.new
    @replies = Reply.joins(:post).joins(:user).select('posts.id as post_id, users.name as name, replies.content as content').where('post_id=?', @post_id)
    #@user = User.find_by(id: session[:user_id])
  end
  def comment
    @content = params[:content]
    @post_id = params[:post_id]
    #puts @post_id
    #puts @content
    
    @reply = Reply.new(post_id: @post_id, user_id: session[:user_id], content: @content)
    
    if @reply.save
      redirect_to(user_top_path)
    else
      render 'new' and return
    end
  end
end
